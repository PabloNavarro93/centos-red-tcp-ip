# Red TCP/IP a trav�s de CentOS minimal #

instalar centOS

clonar la maquina virtual 6 veces, llamadas pc1, pc2, pc3, route1, route2, route3.

agrupar todas las maquinas creadas en un grupo llamado Test-Network

configurar en virtualbox las subredes de cada maquina

Configurar el reenv�o de puertos en las interfaces NAT

Encender la maquina virtual pc1

Ingresar como root

Habilitar la conexion a internet

ifup enp0s3

Instalar paquetes de nano y firewalld

```
# yum install -y nano
```
```
# yum install -y firewalld
```

modificar la ruta /etc/sysconfig/network-scripts/ifcfg-enp0s3

cambiar ONBOOT=yes para tener internet despues del booteo del sistema

ingresar a la ruta /etc/hostname y /etc/hosts

reemplazar localhost.localdomain por pc1

ejecutar el comando 

```
# hostname pc1
```

reiniciar o apagar la m�quina para aplicar cambios.

Realizar lo mismo pero en las dem�s m�quinas virtuales, reemplazando pc1 por sus correspondientes nombres

Ahora en todas las maquinas PC deber� modificar la siguiente ruta
```
# nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
a�adiendo lo siguiente
```
DEVICE=enp0s8
BOOTPROTO=none
ONBOOT=yes
NETMASK=255.255.255.0
IPADDR=192.168.1.1
USERCTL=no
```
y para los router deber� modificar tambien estas, cambiando la variable de DEVICE y IPADDR por las correspondientes a la tabla

```
# nano /etc/sysconfig/network-scripts/ifcfg-enp0s9
```
```
# nano /etc/sysconfig/network-scripts/ifcfg-enp0s10
```

Instalar el traceroute en todas las maquinas, e instalar el quagga solo en las maquinas de router
```
# yum install -y traceroute
```
```
# yum install -y quagga
```
editar /etc/sysconfig/quagga




